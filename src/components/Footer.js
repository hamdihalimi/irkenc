import React from 'react';
import './Footer.css';
import { Button } from './Button';
import { Link } from 'react-router-dom';

function Footer() {
  return (
    <div className='footer-container'>
      <div class='footer-links'>
        <div className='footer-link-wrapper'>
          <div class='footer-link-items'>
            <h2>About Us</h2>
            <Link to='/about'>About Me</Link>
          </div>
          <div class='footer-link-items'>
            <h2>Contact Us</h2>
            <Link to='/contact'>Contact</Link>
            <Link to='/contact'>Support</Link>
          </div>
        </div>
        <div className='footer-link-wrapper'>
          <div class='footer-link-items'>
            <h2>Videos</h2>
            <Link to='/'>Submit Video</Link>
            <Link to='/'>Agency</Link>
          </div>
          <div class='footer-link-items'>
            <h2>Social Media</h2>
            <a href='https://www.instagram.com/irkenc.hyka/' target="_blank">Instagram</a>
            <a href="https://www.facebook.com/" target="_blank">Facebook</a>
            <a href="https://www.youtube.com/channel/UC7be7JLTLZvubrnGNY31ySQ" target="_blank">Youtube</a>
            <a href="https://twitter.com/?lang=en" target="_blank">Twitter</a>
          </div>
        </div>
      </div>
      <section class='social-media'>
        <div class='social-media-wrap'>
          <div class='footer-logo'>
            <a href="https://www.instagram.com/onima.co/?hl=en"   target="_blank" className='social-logo'>
              Onima
            </a>
          </div>
          <small class='website-rights'>Onima © 2021</small>
          <div class='social-icons'>
          <a href="https://www.facebook.com/ONIMAOfficial" target="_blank">
              <i class='fab fa-facebook-f' />
            </a>
            <a href="https://www.instagram.com/onima.co/?hl=en" target="_blank">
              <i class='fab fa-instagram' />
            </a>
            <a href="https://www.youtube.com/c/ONIMA" target="_blank">
              <i class='fab fa-youtube' />
            </a>
            <a href="https://twitter.com/?lang=en" target="_blank">
              <i class='fab fa-twitter' />
            </a>
            <a href="https://www.linkedin.com/company/onimaofficial/" target="_blank">
              <i class='fab fa-linkedin' />
            </a>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Footer;
