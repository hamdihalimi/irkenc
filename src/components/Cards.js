import React from 'react';
import './Cards.css';
import CardItem from './CardItem';
import { Container } from 'react-bootstrap';

function Cards() {
  return (
    <div className='cards'>
      <h1>Muzikët e Fundit</h1>
      <Container>
      <div className='cards__container'>
        <div className='cards__wrapper'>
        <ul className='cards__items'>
            <CardItem
              src='images/irkencdasma.jpg'
              text='Irkenc Hyka ft Remzie Osmani - Dasma'
              label='8 Tetor 2021'
              path='https://www.youtube.com/watch?v=CG-aR4slCak '
            />
            <CardItem
              src='images/irkencxhane.jpg'
              text=' Irkenc Hyka ft Stiv Boka - Xhane'
              label='13 Korrik 2021'
              path='https://www.youtube.com/watch?v=VMBYrPyQSco'
            />
            <CardItem
              src='images/irkencmaria.jpg'
              text='Irkenc Hyka - Maria'
              label= ' 18 Prill 2021'
              path='https://www.youtube.com/watch?v=xGpbucEef04'
            />
          </ul>
          <ul className='cards__items'>
            <CardItem
              src='images/cover.jpg'
              text='Irkenc Hyka - Mka mar malli'
              label='30 Nentor 2020'
              path='https://www.youtube.com/watch?v=9TX5dd71Ggg'
            />
            <CardItem
                src='images/akishezemer.jpg'
                text='Irkenc Hyka - A kishe zemer ?'
                label='25 Shtator 2020'
                path='https://www.youtube.com/watch?v=726H6hjDza8'
            />
             <CardItem
             src='images/menxipres.jpg'
             text='Irkenc Hyka - Mezi Pres'
             label='23 Janar 2020'
             path='https://www.youtube.com/watch?v=kpo0OxJgq3E'
            />
          </ul>
         
        </div>
      </div>
      </Container>
    </div>
  );
}

export default Cards;
