import React from "react";
import "../../App.css";
import Footer from "../Footer";
import HeroSection from "../HeroSection";
import { Form, Row, Col, Container, Button } from "react-bootstrap";

function Contact() {
  return (
    <>
      <HeroSection />
      <div className="contact">
        <Container>
          <h1>Kontakti me Irkenc Hyken</h1>
          <Row>
            <Col sm={6}>
              <Form>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Control type="text" placeholder="Emri" />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Control type="text" placeholder="Mbiemri" />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlInput1"
                >
                  <Form.Control type="email" placeholder="Email" />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlTextarea1"
                >
                  <Form.Control placeholder="Mesazhi" as="textarea" rows={3} />
                </Form.Group>
                <Col
                  className="buttom_contact"
                  style={{
                    backgroundColor: "blue",
                    color: "white",
                    padding: "1rem",
                    textAlign: "center",
                  }}
                  lg={12}
                >
                  <a >Dergo</a>
                </Col>
              </Form>
            </Col>
            <Col className="contact_info" sm={4}>
              <p>Email : irkenchyka@hotmail.com</p>
              <p>Tel : +383 49 ******* </p>
              <p>Rruga e Dursit 12</p>
              <p>Tirane, Shqiperi</p>
            </Col>
          </Row>
        </Container>
      </div>
      <Footer />
    </>
  );
}
export default Contact;
