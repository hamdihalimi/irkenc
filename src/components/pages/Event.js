import React from "react";
import { Container, Accordion, Row, Col } from "react-bootstrap";
import "../../App.css";
import Footer from "../Footer";
import HeroSection from "../HeroSection";
import { BsFacebook, BsInstagram } from "react-icons/bs";

function Event() {
  return (
    <>
      <HeroSection />
      <div className="event">
        <Container className="event-content">
          <h2>Eventet</h2>
          <Row>
            <Col className="event-first" lg={12} sm={12}>
              <Accordion variant="transparent">
                <Accordion.Item eventKey="0">
                  <Accordion.Header>
                    Bio Bio - Yll Baka & Irkenc Hyka
                  </Accordion.Header>
                  <Accordion.Body>
                    <a
                      href="https://www.instagram.com/irkenc.hyka/"
                      target="_blank"
                    >
                      <BsInstagram
                        style={{
                          color: "blue",
                          margin: "1rem",
                          fontSize: "2rem",
                        }}
                      />
                    </a>
                    <a
                      href="https://www.facebook.com/IrkencHykaOfficial"
                      target="_blank"
                    >
                      <BsFacebook style={{ color: "blue", fontSize: "2rem" }} />
                    </a>
                    <br />
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.
                  </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                  <Accordion.Header>
                    Magic 4 - Stresi & Irkenc Hyka
                  </Accordion.Header>
                  <Accordion.Body>
                    <a
                      href="https://www.instagram.com/irkenc.hyka/"
                      target="_blank"
                    >
                      <BsInstagram
                        style={{
                          color: "blue",
                          margin: "1rem",
                          fontSize: "2rem",
                        }}
                      />
                    </a>
                    <a
                      href="https://www.facebook.com/IrkencHykaOfficial"
                      target="_blank"
                    >
                      <BsFacebook style={{ color: "blue", fontSize: "2rem" }} />
                    </a>
                    <br />
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.
                  </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="2">
                  <Accordion.Header>
                    Sheffield - Shkurte Gashi & Astrit & Irkenc
                  </Accordion.Header>
                  <Accordion.Body>
                    <a
                      href="https://www.instagram.com/irkenc.hyka/"
                      target="_blank"
                    >
                      <BsInstagram
                        style={{
                          color: "blue",
                          margin: "1rem",
                          fontSize: "2rem",
                        }}
                      />
                    </a>
                    <a
                      href="https://www.facebook.com/IrkencHykaOfficial"
                      target="_blank"
                    >
                      <BsFacebook style={{ color: "blue", fontSize: "2rem" }} />
                    </a>
                    <br />
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
            </Col>
          </Row>
        </Container>
      </div>
      <Footer />
    </>
  );
}

export default Event;
