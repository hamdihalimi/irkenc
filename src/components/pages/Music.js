import React from "react";
import "../../App.css";
import Footer from "../Footer";
import HeroSection from "../HeroSection";
import { Table, Container } from "react-bootstrap";
import { AiOutlineYoutube } from "react-icons/ai";
import { RiSpotifyLine } from "react-icons/ri";

function Music() {
  return (
    <>
      <HeroSection />

      <div className="allmusic">
        <h1>Te gjitha muziket nga Irkenc Hyka</h1>
        <Container className="table">
          <Table variant="transparent">
            <thead>
              <tr>
                <th>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>#</p>
                </th>
                <th>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka
                  </p>
                </th>
                <th>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    <a
                      href="https://www.youtube.com/watch?v=HPvKiTLjBR0"
                      target="_blank"
                    >
                      Chanel
                    </a>
                  </p>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>1</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Ajo
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=HPvKiTLjBR0"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>2</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Paranormal Love
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=3m_hbt23LUA"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>3</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Lule Pa Ere
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=M9YW81HII0c"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>4</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Mama
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=PlJyc6fCcHc"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>5</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Xhani
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=FGTSw-qMaSE"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>6</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Xhensila - Si Te Jem Mire
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=oMR4ka9TzIo"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>7</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Haram
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=bnfj-qYOb4M"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>8</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Dani - Nje Lamtumire
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=-3_jXc0iYEk"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>9</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Lionel Boci - Pasqyre
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=-lQKd7clNmw"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>10</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Tymi Dashnis
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=Ce8tcAkkiC4"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>11</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Ne
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=R04I-DurRCU"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>12</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Poni - Dashni E Vjeter
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=MwXgVWfirrw"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>12</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Bes Kallaku - Makarena
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=_3yfF4n0UsE"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>13</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Bes Kallaku - Si une
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=R4_q8BNoXJ4"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>14</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Kaltrina Selimi - Kthema
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=iq-Rok6k620"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>15</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Mimoza Shkodra - Valla
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=-JVD_ZtWkkA"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>16</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Katile
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=NYvGw3gUTt4"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>17</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Nora Istrefi - Jena nda
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=2f8CHeFCv2Y"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>18</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Kenge Shpirti
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=ig4MrVVwSe4"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>19</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Don Phenom - A je vetem
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=bfWD6NqXVDk"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>20</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Mezi Pres
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=kpo0OxJgq3E"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>21</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - A kishe zemer
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=726H6hjDza8"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>22</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - A mka mar malli
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=9TX5dd71Ggg"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>23</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka - Dehur{" "}
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=Q0mhu4Ruq24"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>24</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Stiv Boka - Xhane{" "}
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=VMBYrPyQSco"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>25</p>
                </td>
                <td>
                  <p style={{ marginBottom: "0", color: "#0d6efd" }}>
                    Irkenc Hyka ft Remzie Osmani - Dasma
                  </p>
                </td>
                <td>
                  <a
                    href="https://www.youtube.com/watch?v=CG-aR4slCak"
                    target="_blank"
                  >
                    <AiOutlineYoutube
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                  <a href="">
                    <RiSpotifyLine
                      style={{ fontSize: "2rem", marginLeft: "1rem" }}
                    />
                  </a>
                </td>
              </tr>
            </tbody>
          </Table>
        </Container>
      </div>
      <Footer />
    </>
  );
}

export default Music;
