import React from "react";
import "../../App.css";
import Footer from "../Footer";
import { Container, Row, Col } from "react-bootstrap";
import HeroSection from "../HeroSection";

function About() {
  return (
    <>
      <HeroSection />
      <div className="About">
        <h1>Irkenc Hyka</h1>
        <div className="about_content">
          <Container>
            <Row>
              <Col
                className="about-first-content"
                lg={10}
                style={{ textAlign: "start" }}
              >
                <h2>Jeta</h2>
                <p>
                  Irkenc Hyka është një këngetar dhe autor këngesh. Ai lindi më
                  7 tetor 1992 ne Tiranë. Irkenc Hyka ka filluar te merret me
                  muzikë që në moshen 17 vjeçare. Hyka nisi te këndoj qysh si
                  fëmije në festivalet e femijëve. Ka mbaruar "Liçeun Artistik
                  për kanto" në Tiranë dhe ndjek studimet ne Akademine e Arteve
                  në Tiranë , është kompozitor i disa këngeve mjaft të njohura.
                  Karriera e tij ka pasur sukses, per faktin se ka bashkepunuar
                  me kengetare te ndryshëm dhe sot vazhdon te punoje dhe te
                  kompozoj ne studion e tij. Irkenci vazhdon të njihet si
                  artisti qe arrin te pershtatet me artiste te tjere duken
                  arritur frymë të re dhe te suksesshme.
                </p>
              </Col>
              <Col className="about-first-content" lg={2} sm={12}>
                <div class="macbook">
                  <div class="inner">
                    <div class="screen">
                      <div class="face-one">
                        <div class="camera"></div>
                        <div class="display">
                          <div class="shade"></div>
                        </div>
                        <span>MacBook Air</span>
                      </div>
                      <img
                        src="http://www.clker.com/cliparts/i/s/H/f/4/T/apple-logo-white.svg"
                        class="logo"
                      />
                    </div>
                    <div class="body">
                      <div class="face-one">
                        <div class="touchpad"></div>
                        <div class="keyboard">
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key space"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                          <div class="key f"></div>
                        </div>
                      </div>
                      <div class="pad one"></div>
                      <div class="pad two"></div>
                      <div class="pad three"></div>
                      <div class="pad four"></div>
                    </div>
                  </div>
                  <div class="shadow"></div>
                </div>
              </Col>
            </Row>
            <Row>
              <Col lg={6}>
                <img src="images/irkenc1.jpg" />
              </Col>
              <Col lg={6} style={{ textAlign: "start" }}>
                <h2>Kariera</h2>
                <p>
                  Në nentor te vitit 2014, Irkenc Hyka merr pjesë në “Kënga
                  Magjike 2014” me kengën "Ajo".
                  <br />
                  Me date 10 Prill 2015, Irkenc Hyka publikon videoklipin
                  "Paranormal Love", në bashkepunim me Petro Xhori. Muzika e
                  kënges u punua nga vete Irkenc Hyka, teksti u shkrua nga Petro
                  Xhori. Videoklipi u prodhua nga ND Film Pro. Me date 14 Prill
                  2016, Irkenc Hyka publikon videoklipin "Lule Pa Ere". <br />
                  Me pas me 28 Nentor 2016, Irkenc Hyka publikon videoklipin
                  "Mama". Muzika e kenges u punua nga vete Irkenc Hyka, teksti u
                  shkrua nga Irkenc Hyka, Ergi Dini. Videoklipi u prodhua nga
                  Max Production. <br />
                  Me date 3 Janar 2017, Irkenc Hyka publikon videoklipin
                  "Xhani". Muzika e kenges u punua nga vete Irkenc Hyka, teksti
                  u shkrua nga Irkenc Hyka, Petro Xhori, ndersa per orkestrimin
                  u kujdes Dj Jojo. Videoklipi u prodhua nga ND Film Pro. <br />
                  “Si te jem mire”, kenga qe beri buje qe nga 28 korriku i vitit
                  2017 e cila ishte ne bashkepunim me Xhensila Myrtezaj. Muzika
                  e kenges u punua nga vete Irkenc Hyka, teksti u shkrua nga
                  Petro Xhori, ndersa per orkestrimin u kujdes Dj Jojo.
                  Videoklipi u prodhua nga Max Production. <br />
                  Me date 8 Shtator 2017, Irkenc Hyka publikon videoklipin
                  "Haram". Muzika e kenges u punua nga vete Irkenc Hyka, teksti
                  u shkrua nga Petro Xhori, ndersa per orkestrimin u kujdes Dj
                  Jojo. Videoklipi u prodhua nga ND Film Pro. <br />
                  Me date 12 Nentor 2017, Irkenc Hyka publikon videoklipin "Nje
                  Lamtumire", ne bashkepunim me Dani. Muzika e kenges u punua
                  nga vete Irkenc Hyka, teksti u shkrua nga Endrit Mumajesi,
                  ndersa per orkestrimin u kujdes Dj Jojo. Videoklipi u prodhua
                  nga Max Production. <br />
                </p>
              </Col>
            </Row>
            <Row>
              <Col style={{ textAlign: "start" }}>
                <p>
                  Me date 28 Shkurt 2018, Irkenc Hyka publikon videoklipin
                  "Pasqyre", ne bashkepunim me Lionel Boci. Videoklipi u prodhua
                  nga kompania Imagine Films.
                  <br />
                  Me date 4 Mars 2018, Irkenc Hyka publikon videoklipin "Tymi
                  Dashnis". Muzika e kenges u punua nga vete Irkenc Hyka, teksti
                  u shkrua nga Petro Xhori, Endrit Mumajesi, ndersa per
                  orkestrimin u kujdes Dj Jojo. Videoklipi u prodhua nga MAX
                  Production. <br />
                  Me date 15 Prill 2018, Irkenc Hyka publikon videoklipin "Ne".
                  Muzika e kenges u punua nga vete Irkenc Hyka, teksti u shkrua
                  nga Endrit Mumajesi, ndersa per orkestrimin u kujdes Dj Jojo.
                  Videoklipi u prodhua nga ND Film Pro. <br />
                  Me date 18 Maj 2018, Irkenc Hyka publikon videoklipin "Dashni
                  E Vjeter", ne bashkepunim me Poni. Muzika e kenges u punua nga
                  vete Irkenc Hyka, teksti u shkrua nga Irkenc Hyka, Endrit
                  Mumajesi, ndersa per orkestrimin u kujdes Dj Jojo. Videoklipi
                  u prodhua nga Imagine Films. <br />
                  Me date 16 Korrik 2018, Irkenc Hyka & Bes Kallaku publikojne
                  videoklipin "Makarena". Muzika e kenges u punua nga Irkenc
                  Hyka, teksti u shkrua nga Endrit Mumajesi, Petro Xhori, Bes
                  Kallaku. <br />
                  Me date 16 Shtator 2018, Irkenc Hyka publikon videoklipin "Si
                  Une". Muzika e kenges u punua nga vete Irkenc Hyka, teksti u
                  shkrua nga Endrit Mumajesi, ndersa per orkestrimin u kujdes Dj
                  Jojo. Videoklipi u prodhua nga kompania Imagine Films. Me date
                  8 Nentor 2018, Irkenc Hyka publikon videoklipin "Kthema", ne
                  bashkepunim me Kaltrina Selimi. Muzika e kenges u punua nga
                  vete Irkenc Hyka, teksti u shkrua nga Endrit Mumajesi, ndersa
                  per orkestrimin u kujdes Dj Jojo. Videoklipi u prodhua nga
                  kompania Imagine Films.
                  <br />
                  Me date 2 Shkurt 2019, Irkenc Hyka publikon videoklipin
                  "Valla", ne bashkepunim me Mimoza Shkodren dhe eshte kujdesur
                  vete per muziken e kenges. Teksti i kenges u shkrua nga Gent
                  Fatali, ndersa orkestrimi u punua nga Dj Jojo. Videoklipi u
                  prodhua nga kompania MAX Production. <br />
                  Me date 23 Shkurt 2019, Irkenc Hyka publikon videoklipin
                  "Katile". Muzika e kenges u punua nga Irkenc Hyka, teksti u
                  shkrua nga Endrit Mumajesi, ndersa per orkestrimin u kujdes Dj
                  Jojo. Videoklipi u prodhua nga kompania Imagine Films.
                  <br />
                  Me date 27 Korrik 2019, Irkenc Hyka publikon videoklipin "Jena
                  Nda", ne bashkepunim me Nora Istrefin. Muzika dhe teksti i
                  kenges jane punuar nga Irkenc Hyka. Videoklipi u prodhua nga
                  kompania Imagine Films.
                  <br />
                  Me date 21 Gusht 2019, Irkenc Hyka publikon videoklipin "Kenge
                  Shpirti". Muzika e kenges u punua nga Irkenc Hyka, ndersa
                  teksti u shkrua nga Endrit Mumajesi. <br />
                  Me date 12 Dhjetor 2019, Irkenc Hyka publikon videoklipin "A
                  Je Vetem", ne bashkepunim me Don Phenom. Muzika e kenges u
                  punua nga Irkenc Hyka, ndersa teksti u shkrua nga Endrit
                  Mumajesi & Don Phenom. <br />
                  Me date 23 Janar 2020, Irkenc Hyka publikon videoklipin "Mezi
                  Pres". Muzika e kenges u punua nga Irkenc Hyka, teksti u
                  shkrua nga Endrit Mumajesi, ndersa per orkestrimin u kujdes
                  Irkenc Hyka & Dj Jojo. Videoklipi u prodhua nga kompania MAX
                  Production. <br />
                  Më datë 28 Maj 2020, Irkenc Hyka publikon videoklipin "Vonë".
                  Muzika e këngës u punua nga Irkenc Hyka, ndërsa teksti u
                  shkrua nga Endrit Mumajesi. Videoklipi u prodhua nga kompania
                  MAX Production.
                  <br />
                  Ne date 25 Shtator 2020, Irkenc hyka publikon videoklipin" A
                  kishe zemer?”. Muzika u shkrua nga vete Irkenci, teksti u
                  shkrua nga Irkenci, ndersa orkestrimin e beri Dj Jojo.
                  Videoklipi u prodhuar nga MAX production dhe pjese e
                  videoklipit ishte edhe kengetarja e njohur Zaimina Vasjari.
                  <br />
                  Më datë 30 Nëntor 2020, Irkenc Hyka publikon videoklipin "M'Ka
                  Marre Malli". Muzika e këngës u punua nga Irkenc Hyka, ndërsa
                  teksti u shkrua nga Endrit Mumajesi & Irkenc Hyka. Videoklipi
                  u prodhua nga kompania Imagine Films.
                  <br />
                  Më datë 26 Shkurt 2021, Irkenc Hyka publikon videoklipin
                  "Dehur". Muzika dhe orkestrimi i këngës janë punuar nga Irkenc
                  Hyka.
                  <br />
                  Më datë 18 Prill 2021, Irkenc Hyka publikon videoklipin
                  "Maria". Muzika dhe teksti i këngës janë punuar nga Irkenc
                  Hyka. Videoklipi u prodhua nga kompania MAX Production.
                  <br />
                  Në Qershor 2021, Irkenc Hyka merr pjesë në Netët E Klipit
                  Shqiptar 21 me këngën "Dehur".
                  <br />
                  Më datë 13 Korrik 2021, Stiv Boka publikon videoklipin
                  "Xhane", në bashkëpunim me Irkenc Hykën. Muzika dhe teksti i
                  këngës janë punuar nga Irkenc Hyka. Videoklipi u prodhua nga
                  kompania Imagine Films.
                  <br />
                  Më datë 8 Tetor 2021, Irkenc Hyka publikon videoklipin
                  "Dasma", në bashkëpunim me Remzie Osmanin. Muzika e këngës u
                  punua nga Irkenc Hyka, ndërsa teksti u shkrua nga Irkenc Hyka
                  & Aida Baraku. Videoklipi u prodhua nga Valon Aziri.
                  <br />
                </p>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default About;
