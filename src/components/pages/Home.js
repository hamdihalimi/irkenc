import React from "react";
import "../../App.css";
import Cards from "../Cards";
import HeroSection from "../HeroSection";
import Footer from "../Footer";
import CardItem from "../CardItem";
import { Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

function Home() {
  return (
    <>
      <HeroSection />
      <Cards />
      <div className="Event">
        <h1>Eventet</h1>
        <Container>
          <div className="cards__container">
            <div className="cards__wrapper">
              <ul className="cards__items">
                <Link className="cards__item__link" to="/event">
                  <CardItem
                    src="images/event1.jpeg"
                    text="Bio Bio - Yll  Baka & Irkenc Hyka"
                    label="10-Mars-2021"
                  />
                </Link>
                <Link className="cards__item__link" to="/event">
                  <CardItem
                    src="images/event2.jpeg"
                    text="Magic 4 - Stresi & Irkenc Hyka"
                    label="15 -Shkurt-2021"
                  />
                </Link>
                <Link className="cards__item__link" to="/event">
                  <CardItem
                    src="images/event3.jpeg"
                    text="Sheffield - Shkurte Gashi & Astrit & Irkenc "
                    label="4-Mars-2021"
                  />
                </Link>
              </ul>
            </div>
          </div>
        </Container>
      </div>

      <div className="About">
        <h1>Jeta dhe Karriera</h1>
        <div className="about_content">
          <Container>
            <Row>
              <Col lg={6} style={{ textAlign: "start" }}>
                <p>
                  Irkenc Hyka është një këngetar dhe autor këngesh. Ai lindi më
                  7 tetor 1992 ne Tiranë. Irkenc Hyka ka filluar te merret me
                  muzikë që në moshen 17 vjeçare. Hyka nisi te këndoj qysh si
                  fëmije në festivalet e femijëve. Ka mbaruar "Liçeun Artistik
                  për kanto" në Tiranë dhe ndjek studimet ne Akademine e Arteve
                  në Tiranë , është kompozitor i disa këngeve mjaft të njohura.
                  Karriera e tij ka pasur sukses, per faktin se ka bashkepunuar
                  me kengetare te ndryshëm dhe sot vazhdon te punoje dhe te
                  kompozoj ne studion e tij. Irkenci vazhdon të njihet si
                  artisti qe arrin te pershtatet me artiste te tjere duken
                  arritur frymë të re dhe te suksesshme.
                  <Link to="/about">See more</Link>
                </p>
              </Col>
              <Col lg={6}>
                <img src="images/irkenc1.jpg" />
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default Home;
