import React from "react";
import "../App.css";
import { Button } from "./Button";
import "./HeroSection.css";

function HeroSection() {
  return (
    <div className="hero-container">
      <video
        src="/videos/video-1.mp4"
        autoPlay={true}
        type="video/mp4"
        loop={true}
        controls={false}
        playsInline
        muted
        disablePictureInPicture={true}
      />
      <h1>Irkenc Hyka</h1>
      <div className="hero-btns">
        <a
          href="https://www.youtube.com/channel/UC7be7JLTLZvubrnGNY31ySQ"
          target="_blank"
        >
          <Button className="btn--outline">
            Youtube Channel <i className="far fa-play-circle" />
          </Button>
        </a>
      </div>
    </div>
  );
}

export default HeroSection;
